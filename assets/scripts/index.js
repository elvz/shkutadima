function showHide(name) {
  var i;
  var x = document.getElementsByClassName("service-text");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(name).style.display = "block";
}

function openFeedback(clientName) {
  var i;
  var x = document.getElementsByClassName("feedback-text");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(clientName).style.display = "block";
}
